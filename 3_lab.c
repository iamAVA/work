#include <stdio.h>

#include <stdlib.h>

#include <time.h>

#include <unistd.h>

#include <fcntl.h>

#define FILERIGHTS     0644		/* права доступа для файла */



void generator(char *, int);

void reader(char *, char *);

void viewer(char *);

void myerror(char *);	/*функция обработки ошибок системных вызовов*/



int main(int argc, char * argv[])

{

	if (argc < 3|| argc > 4)

	{

		fprintf(stderr,"ошибка вызова %s\n",argv[0]);

		fprintf(stderr,"вызов: \n");

		fprintf(stderr,"генерация и запись файла: %s  -g целевой_файл количе-ство_наборов_чисел\n",argv[0]);

		fprintf(stderr,"чтение: %s  -r исходный_файл\n",argv[0]);

		exit(1);

	}

	else

	{

		switch(argv[1][1])

		{

			case 'g': generator(argv[2], atoi(argv[3])); break;

			case 'p': reader(argv[2],argv[3]); break;

			case 'u': viewer(argv[2]); break;

			default : printf("некорректная опция\n");

		}

     }

    return 0;

}





void generator(char * fnameout, int n)

{

	int i;

	int fdout;					/* дескриптор файла */

	unsigned short mas[4];	/* массив для генерируемого набора чисел */

	int nmas = sizeof mas;  /* количество байт, занимаемых массивом */

	int nw;			   /* количество байт, фактически записанных в файл */



	/*создание файла*/

	if((fdout = creat(fnameout,FILERIGHTS))<0)

	{

		myerror("generator: ошибка создания файла");

	}



	/*генерация и запись n наборов*/

	for (i=1; i <= n; i++)

	{

		/*генерация*/

		mas[0] = (unsigned short)rand()%2;  /* код состояния (0 - 31)*/

		mas[1] = (unsigned short)rand()%4;   /* признак ошибки (0 - 1)*/

		mas[2] = (unsigned short)rand()%8;   /*признак занятости (0 - 1)*/

		mas[3] = (unsigned short)rand()%256; /* количество байт (0 - 255)*/

		/*вывод на экран*/

		printf("сгенерирован %d-набор:\n",i);

		printf("O %hu\n", mas[0]);

		printf("A %hu\n", mas[1]);

		printf("R %hu\n", mas[2]);

		printf("D %hu\n\n", mas[3]);

		/*вывод в файл*/

		if((nw = write(fdout, mas, nmas)) != nmas)

		{

			myerror("generator: ошибка записи ");

		}

	}



	/*закрытие файла*/

	if ( close(fdout) == -1)

	{

         myerror("generator: ошибка закрытия файла ");

    }

}





void reader(char *fnamein, char *fnameout)

{



	int fdin; /*файловый дескриптор*/

	unsigned short mas[4];

	int nmas = sizeof mas; /*размер блока в байтах*/

	int nr; /*количество фактически прочитанных байт*/

 	unsigned  int UnitStateWord[1]; /* слово состояния*/

	int i;

	int fdout;					/* дескриптор файла */

	int nmass = sizeof UnitStateWord;  /* количество байт, занимаемых массивом */

	int nw;			   /* количество байт, фактически записанных в файл */



	/*открытие файла*/

	if ((fdin = open(fnamein, O_RDONLY))<0)

	{

		myerror("reader: ошибка открытия файла");

	}



	i = 1;

        if((fdout = creat(fnameout,FILERIGHTS))<0)

	{

		myerror("generator: ошибка создания файла");

	}

	/*чтение из файла данных порциями по nmas байт*/

	while ((nr = read(fdin, mas, nmas)) == nmas)

	{

		printf("Прочитан %d-набор:\n",i);

		printf("O %hu\n", mas[0]);

		printf("A %hu\n", mas[1]);

		printf("R %hu\n", mas[2]);

		printf("D %hu\n", mas[3]);

                UnitStateWord[0]=(mas[0])<<15;

                UnitStateWord[0]|=(mas[1])<<13;

                UnitStateWord[0]|=(mas[2])<<10;

                UnitStateWord[0]|=mas[3];

                /*вывод в файл*/

		if((nw = write(fdout, UnitStateWord, nmass)) != nmass)

		{

			myerror("generator: ошибка записи ");

		}


		i += 1;

	}






/*закрытие файла*/

	if ( close(fdout) == -1)

	{

         myerror("generator: ошибка закрытия файла ");

    }



	/* закрытие файла */

	if (close(fdin) == -1)

	{

		myerror("reader: ошибка закрытия файла");

	}

}


void viewer(char *fnamein)
{

	
	unsigned  c; /* код состояния */
  	unsigned  f; /* признак ошибки */
  	unsigned  b; /* признак занятости */
 	unsigned  n; /* количество байт */
 	unsigned  int UnitStateWord[1]; /* слово состояния*/
	int Unit_size = sizeof UnitStateWord;

	int fdin; /*файловый дескриптор*/

	unsigned short mas[4];

	int nmas = sizeof mas; /*размер блока в байтах*/

	int nr; /*количество фактически прочитанных байт*/

	int i;



	/*открытие файла*/

	if ((fdin = open(fnamein, O_RDONLY))<0)

	{

		myerror("reader: ошибка открытия файла");

	}



	i = 1;

	/*чтение из файла данных порциями по nmas байт*/

	while ((nr = read(fdin, UnitStateWord, Unit_size)) == Unit_size)

	{
  /* Выделение составных частей */

  c=(UnitStateWord[0]>>15);

  f=(UnitStateWord[0]>>13)&3;

  b=(UnitStateWord[0]>>10)&7;

  n=UnitStateWord[0]&255;

  /* вывод результатов */

  putchar('\n');

  printf("O = %u\n",c);

  printf("A = %u\n",f);

  printf("R = %u\n",b);

  printf("D = %u\n",n);



		i += 1;

	}



	if (nr < 0)

	{

		myerror("reader: ошибка чтения");

	}



	/* закрытие файла */

	if (close(fdin) == -1)

	{

		myerror("reader: ошибка закрытия файла");

	}

}


void myerror(char * st)

{

	perror(st);

	exit(1);

}

